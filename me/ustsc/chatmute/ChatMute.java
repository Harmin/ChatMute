/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ustsc.chatmute;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Trent
 */
public class ChatMute extends JavaPlugin {
    
    private boolean chatMute;
    
    @Override
    public void onEnable(){
        
    }
    
    @Override
    public boolean onCommand(CommandSender cs, Command c, String s, String[] sa) {
        if (c.getName().equalsIgnoreCase("chatmute")){
            if (sa.length == 0) {
                return true;
            }
            if (!this.isInteger(sa[0])) {
                return true;
            }
            this.chatMute = true;
            this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
                @Override
                public void run() {
                    chatMute = false;
                }
            }, Integer.parseInt(sa[0]));
        }
        return true;
    }
    
    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex) {
        }
        return false;
    }
    
    @EventHandler
    public void onChat(AsyncPlayerChatEvent e){
        if (this.chatMute){
            e.setCancelled(true);
            e.getPlayer().sendMessage("muted");
        }
    }
    
}
